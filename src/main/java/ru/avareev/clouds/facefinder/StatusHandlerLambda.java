package ru.avareev.clouds.facefinder;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StatusHandlerLambda implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {
    private static final String dbTableName = "faceFinder";

    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent event, Context context) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Access-Control-Allow-Origin", "*");
        headers.put("Access-Control-Allow-Methods", "OPTIONS, POST, GET");
        headers.put("Access-Control-Allow-Headers", "*");

        AmazonDynamoDB client = Helpers.buildDynamoClient();
        DynamoDB dynamoDB = new DynamoDB(client);
        Table table = dynamoDB.getTable(dbTableName);
        Item item = table.getItem("ID", 0);
        if (item == null) return new APIGatewayProxyResponseEvent()
                .withStatusCode(404)
                .withBody("Item not found")
                .withHeaders(headers);

        String jobId = item.getString("JobId");
        String status = item.getString("Status");
        List<Map<String, ?>> data = item.getList("Data");
        StatusResponse response = new StatusResponse(jobId, status, data);

        ObjectMapper mapper = new ObjectMapper();
        String body;
        try {
            body = mapper.writeValueAsString(response);
        } catch (JsonProcessingException e) {
            System.out.println("ERROR");
            e.printStackTrace();
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(500)
                    .withBody("Error when serialize json")
                    .withHeaders(headers);
        }

        headers.put("Content-Type", "application/json");
        return new APIGatewayProxyResponseEvent()
                .withStatusCode(200)
                .withBody(body)
                .withHeaders(headers);
    }
}
