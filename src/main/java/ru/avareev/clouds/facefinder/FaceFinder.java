package ru.avareev.clouds.facefinder;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.AmazonRekognitionClientBuilder;
import com.amazonaws.services.rekognition.model.*;

import java.util.List;

public class FaceFinder {
    private static final String ACCESS_KEY = "AKIAI73VBSA32YCJXQ5A";
    private static final String SECRET_KEY = "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";

    private static final BasicAWSCredentials awsCreds = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);

    private static AmazonRekognition buildRekognitionClient() {
        return AmazonRekognitionClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion(Regions.US_EAST_2)
                .build();
    }

    private static void deleteCollection(String collectionId){
        AmazonRekognition rekognitionClient = buildRekognitionClient();
        DeleteCollectionRequest request = new DeleteCollectionRequest().withCollectionId(collectionId);
        DeleteCollectionResult deleteCollectionResult = rekognitionClient.deleteCollection(request);
        System.out.printf("delete collection %s result: %d%n", collectionId, deleteCollectionResult.getStatusCode());
    }

    /**
     * 1st step is creating a collection and note the collection identifier you used
     *
     * @param collectionId -- Collection Id. For example, "MyCollection"
     */
    private static void createCollection(String collectionId) {

//        AmazonRekognition rekognitionClient = AmazonRekognitionClientBuilder.defaultClient();
        AmazonRekognition rekognitionClient = buildRekognitionClient();
        System.out.println("Creating collection: " + collectionId);

        CreateCollectionRequest request = new CreateCollectionRequest()
                .withCollectionId(collectionId);

        CreateCollectionResult createCollectionResult = rekognitionClient.createCollection(request);
        System.out.println("CollectionArn : " + createCollectionResult.getCollectionArn());
        System.out.println("Status code : " + createCollectionResult.getStatusCode().toString());
    }

    /**
     * 2nd step -- Index the faces you want to search for into the collection you created in step 2.
     *
     * @param collectionId -- Example: "MyCollection"
     * @param bucket       -- Example: "bucket"
     * @param photo        -- Example: "input.jpg"
     */
    private static void indexFaces(String collectionId, String bucket, String photo, String externalImageId) {
        AmazonRekognition rekognitionClient = buildRekognitionClient();

        Image image = new Image()
                .withS3Object(new S3Object()
                        .withBucket(bucket)
                        .withName(photo));

        IndexFacesRequest indexFacesRequest = new IndexFacesRequest()
                .withImage(image)
                .withQualityFilter(QualityFilter.AUTO)
                .withMaxFaces(1)
                .withCollectionId(collectionId)
                .withExternalImageId(externalImageId)
                .withDetectionAttributes("DEFAULT");

        IndexFacesResult indexFacesResult = rekognitionClient.indexFaces(indexFacesRequest);

        System.out.println("Results for " + photo);
        System.out.println("Faces indexed:");
        List<FaceRecord> faceRecords = indexFacesResult.getFaceRecords();
        for (FaceRecord faceRecord : faceRecords) {
            System.out.println("  Face ID: " + faceRecord.getFace().getFaceId());
            System.out.println("  Location:" + faceRecord.getFaceDetail().getBoundingBox().toString());
        }

        List<UnindexedFace> unindexedFaces = indexFacesResult.getUnindexedFaces();
        System.out.println("Faces not indexed:");
        for (UnindexedFace unindexedFace : unindexedFaces) {
            System.out.println("  Location:" + unindexedFace.getFaceDetail().getBoundingBox().toString());
            System.out.println("  Reasons:");
            for (String reason : unindexedFace.getReasons()) {
                System.out.println("   " + reason);
            }
        }
    }

    public static void deleteFaces(String collectionId, String[] faces) {
        AmazonRekognition rekognitionClient = buildRekognitionClient();

        DeleteFacesRequest deleteFacesRequest = new DeleteFacesRequest()
                .withCollectionId(collectionId)
                .withFaceIds(faces);

        DeleteFacesResult deleteFacesResult=rekognitionClient.deleteFaces(deleteFacesRequest);


        List < String > faceRecords = deleteFacesResult.getDeletedFaces();
        System.out.println(Integer.toString(faceRecords.size()) + " face(s) deleted:");
        for (String face: faceRecords) {
            System.out.println("FaceID: " + face);
        }
    }


    public static void main(String[] args) {
        String collectionId = "FacefinderFaces";
        String bucket = "facefinder.ddns.net";
        String video = "data/input.mp4";
        String face = "data/face.jpg";
        String externalImageId = "face";
        deleteCollection(collectionId);
        createCollection(collectionId);
        indexFaces(collectionId, bucket, face, externalImageId); // => a572f072-fdf6-4f5a-80e7-88a9e1b3da2d
    }
}
