package ru.avareev.clouds.facefinder;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.amazonaws.services.rekognition.model.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.*;

@SuppressWarnings("Duplicates")
public class ResultHandlerLambda implements RequestHandler<SQSEvent, String> {
    private static final String dbTableName = "faceFinder";

    @Override
    public String handleRequest(SQSEvent event, Context context) {
        try {
            List<SQSEvent.SQSMessage> messages = event.getRecords();
            if (messages.isEmpty()) {
                System.out.println("Empty sqs");
                return "BAD";
            }
            String jobId = getJobId();
            for (SQSEvent.SQSMessage message : messages) {
                String notification = message.getBody();

                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonMessageTree = mapper.readTree(notification);
                JsonNode messageBodyText = jsonMessageTree.get("Message");
                ObjectMapper operationResultMapper = new ObjectMapper();
                JsonNode jsonResultTree = operationResultMapper.readTree(messageBodyText.textValue());
                JsonNode operationJobId = jsonResultTree.get("JobId");
                JsonNode operationStatus = jsonResultTree.get("Status");
                System.out.println("Job found was " + operationJobId);

                if (operationJobId.asText().equals(jobId)) {
                    System.out.println("Our job found");
                    System.out.println("Job id: " + operationJobId);
                    System.out.println("Status : " + operationStatus.toString());
                    if (operationStatus.asText().equals("SUCCEEDED")) {
                        GetResultsFaceSearchCollection(jobId);
                    } else {
                        storeFinishedInDB(operationStatus.asText(), jobId, Collections.emptyList());
                        System.out.println("Video analysis failed");
                    }
                } else {
                    System.out.println("Not our job detected");
                }
            }
        } catch (Exception e) {
            System.out.println("ERROR");
            e.printStackTrace();
        }
        return "OK";
    }

    private String getJobId() {
        AmazonDynamoDB client = Helpers.buildDynamoClient();
        DynamoDB dynamoDB = new DynamoDB(client);
        Table table = dynamoDB.getTable(dbTableName);
        Item jobInfo = table.getItem("ID", 0);
        if (jobInfo == null) return null;
        return jobInfo.getString("JobId");
    }

    private static void GetResultsFaceSearchCollection(String startJobId) {
        GetFaceSearchResult faceSearchResult = null;
        int maxResults = 10;
        String paginationToken = null;
        List<Map<String, ?>> resultData = new ArrayList<>();

        do {
            if (faceSearchResult != null) {
                paginationToken = faceSearchResult.getNextToken();
            }
            faceSearchResult = Helpers.buildRekognitionClient().getFaceSearch(
                    new GetFaceSearchRequest()
                            .withJobId(startJobId)
                            .withMaxResults(maxResults)
                            .withNextToken(paginationToken)
                            .withSortBy(FaceSearchSortBy.TIMESTAMP)
            );

            //Show search results
            List<PersonMatch> matches = faceSearchResult.getPersons();
            for (PersonMatch match : matches) {
                List<FaceMatch> faceMatches = match.getFaceMatches();
                if (faceMatches != null) {
                    long milliSeconds = match.getTimestamp();
                    System.out.print("Timestamp: " + Long.toString(milliSeconds));
                    System.out.println(" Person number: " + match.getPerson().getIndex());
                    for (FaceMatch faceMatch : faceMatches) {
                        Face face = faceMatch.getFace();
                        System.out.println("Face Id: " + face.getFaceId());
                        System.out.println("Similarity: " + faceMatch.getSimilarity().toString());
                        BoundingBox box = face.getBoundingBox();
                        System.out.printf("Box: top=%s left=%s height=%s width=%s%n", box.getTop(), box.getLeft(), box.getHeight(), box.getWidth());
                        System.out.println();
                        resultData.add(makeFaceResult(faceMatch, match.getTimestamp()));
                    }
                }
            }
        } while (faceSearchResult != null && faceSearchResult.getNextToken() != null);
        storeFinishedInDB("SUCCEEDED", startJobId, resultData);
    }

    private static Map<String, ?> makeFaceResult(FaceMatch faceMatch, long timestamp) {
        Face face = faceMatch.getFace();
        Map<String, Object> result = new HashMap<>();
        result.put("faceId", face.getFaceId());
        result.put("similarity", faceMatch.getSimilarity().toString());
        result.put("timestamp", timestamp);
        result.put("box", boxToMap(face.getBoundingBox()));
        return result;
    }

    private static Map<String, Float> boxToMap(BoundingBox box) {
        Map<String, Float> result = new HashMap<>();
        result.put("top", box.getTop());
        result.put("left", box.getLeft());
        result.put("width", box.getWidth());
        result.put("height", box.getHeight());
        return result;
    }

    private static void storeFinishedInDB(String status, String jobId, List<Map<String, ?>> resultData) {
        System.out.println("Storing " + status + " into dynamoDB");
        AmazonDynamoDB client = Helpers.buildDynamoClient();
        DynamoDB dynamoDB = new DynamoDB(client);
        Table table = dynamoDB.getTable(dbTableName);
        table.deleteItem("ID", 0);
        Item item = new Item()
                .withPrimaryKey("ID", 0)
                .withString("Status", status)
                .withString("JobId", jobId)
                .withList("Data", resultData);

        table.putItem(item);
    }
}
