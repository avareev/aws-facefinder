package ru.avareev.clouds.facefinder;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.model.*;
import com.amazonaws.services.sqs.AmazonSQS;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("Duplicates")
public class FindFacesHandlerLambda implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {
    private static String COLLECTION_ID = "FacefinderFaces";
    private static String BUCKET = "facefinder.ddns.net";
    private String video = "data/input.mp4";
    private String face = "data/face.jpg";
    private static final String dbTableName = "faceFinder";
    private static String topicArn = "arn:aws:sns:us-east-2:959263463332:facefinder";
    private static String roleArn = "arn:aws:iam::959263463332:role/RekognitionRole";

    private static AmazonSQS sqs = null;
    private static AmazonRekognition rek = null;

    private static NotificationChannel channel = new NotificationChannel()
            .withSNSTopicArn(topicArn)
            .withRoleArn(roleArn);


    private static Map<String, String> headers = new HashMap<>();

    static {
        headers.put("Content-Type", "application/json");
        headers.put("Access-Control-Allow-Origin", "*");
        headers.put("Access-Control-Allow-Methods", "OPTIONS, POST, GET");
        headers.put("Access-Control-Allow-Headers", "*");
    }

    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent event, Context context) {
        if (!event.getHttpMethod().equals("POST")) {
            return new APIGatewayProxyResponseEvent()
                    .withBody("Only POST method supported")
                    .withStatusCode(400)
                    .withHeaders(headers);
        }
        try {
            ObjectMapper mapper = new ObjectMapper();
            FindFaceRequest requestData = mapper.readValue(event.getBody(), FindFaceRequest.class);
            face = requestData.faceImage;
            video = requestData.video;
        } catch (Exception e) {
            System.out.println("ERROR IN PARAM DESERIALIZATION");
            e.printStackTrace();
            return new APIGatewayProxyResponseEvent().
                    withStatusCode(400)
                    .withBody("Invalid request body. Waited for {'video':'', 'faceImage': ''}. Have: " + event.getBody())
                    .withHeaders(headers);
        }
        String externalImageId = "face";
        deleteCollection(COLLECTION_ID);
        createCollection(COLLECTION_ID);
        boolean faceRecognized = indexFaces(COLLECTION_ID, BUCKET, face, externalImageId);
        if (!faceRecognized) {
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(400)
                    .withBody("Face not found at image")
                    .withHeaders(headers);
        }

        sqs = Helpers.buildSQSClient();
        rek = Helpers.buildRekognitionClient();
        try {
            StartFaceSearchCollection(BUCKET, video);
        } catch (Exception e) {
            System.out.println("Error in StartFaceSearchCollection");
            e.printStackTrace();
        }
        return new APIGatewayProxyResponseEvent()
                .withStatusCode(200)
                .withBody("Started")
                .withHeaders(headers);
    }

    private void deleteCollection(String collectionId) {
        AmazonRekognition rekognitionClient = Helpers.buildRekognitionClient();
        DeleteCollectionRequest request = new DeleteCollectionRequest().withCollectionId(collectionId);
        DeleteCollectionResult deleteCollectionResult = rekognitionClient.deleteCollection(request);
        System.out.printf("delete collection %s result: %d%n", collectionId, deleteCollectionResult.getStatusCode());
    }

    private static void createCollection(String collectionId) {
        AmazonRekognition rekognitionClient = Helpers.buildRekognitionClient();

        CreateCollectionRequest request = new CreateCollectionRequest()
                .withCollectionId(collectionId);

        CreateCollectionResult createCollectionResult = rekognitionClient.createCollection(request);
        System.out.println("CollectionArn : " + createCollectionResult.getCollectionArn());
        System.out.println("Status code : " + createCollectionResult.getStatusCode().toString());
    }

    private static boolean indexFaces(String collectionId, String bucket, String photo, String externalImageId) {
        AmazonRekognition rekognitionClient = Helpers.buildRekognitionClient();

        Image image = new Image()
                .withS3Object(new S3Object()
                        .withBucket(bucket)
                        .withName(photo));

        IndexFacesRequest indexFacesRequest = new IndexFacesRequest()
                .withImage(image)
                .withQualityFilter(QualityFilter.AUTO)
                .withMaxFaces(1)
                .withCollectionId(collectionId)
                .withExternalImageId(externalImageId)
                .withDetectionAttributes("DEFAULT");

        IndexFacesResult indexFacesResult = rekognitionClient.indexFaces(indexFacesRequest);
        List<FaceRecord> faceRecords = indexFacesResult.getFaceRecords();
        System.out.printf("Recognized %d faces%n", faceRecords.size());
        return faceRecords.size() > 0;
    }

    private static void StartFaceSearchCollection(String bucket, String video) {
        StartFaceSearchRequest req = new StartFaceSearchRequest()
                .withCollectionId(COLLECTION_ID)
                .withVideo(new Video()
                        .withS3Object(new S3Object()
                                .withBucket(bucket)
                                .withName(video)))
                .withNotificationChannel(channel);

        StartFaceSearchResult startPersonCollectionSearchResult = rek.startFaceSearch(req);
        String jobId = startPersonCollectionSearchResult.getJobId();
        System.out.println("Search started. " + startPersonCollectionSearchResult.toString());
        storeRunningInDB(jobId);
    }

    private static void storeRunningInDB(String jobId) {
        AmazonDynamoDB client = Helpers.buildDynamoClient();
        DynamoDB dynamoDB = new DynamoDB(client);
        Table table = dynamoDB.getTable(dbTableName);
        table.deleteItem("ID", 0);
        Item item = new Item()
                .withPrimaryKey("ID", 0)
                .withString("Status", "RUNNING")
                .withString("JobId", jobId)
                .withList("Data", Collections.emptyList());

        table.putItem(item);
    }
}
