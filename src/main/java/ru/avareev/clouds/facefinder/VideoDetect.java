package ru.avareev.clouds.facefinder;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.model.*;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.Message;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.*;

public class VideoDetect {

    private static String COLLECTION_ID = "FacefinderFaces";

    private static String bucket = "facefinder.ddns.net";
    private static String video = "data/input2.mp4";
    private static String queueUrl = "https://sqs.us-east-2.amazonaws.com/959263463332/facefinder";
    private static String topicArn = "arn:aws:sns:us-east-2:959263463332:facefinder";
    private static String roleArn = "arn:aws:iam::959263463332:role/RekognitionRole";
    private static AmazonSQS sqs = null;
    private static AmazonRekognition rek = null;

    private static NotificationChannel channel = new NotificationChannel()
            .withSNSTopicArn(topicArn)
            .withRoleArn(roleArn);

    private static String startJobId = null;

    public static void main(String[] args) throws Exception {
        sqs = Helpers.buildSQSClient();
        rek = Helpers.buildRekognitionClient();


        //=================================================
        StartFaceSearchCollection(bucket,video);
        //=================================================
        System.out.println("Waiting for job: " + startJobId);
        //Poll queue for messages
        List<Message> messages = null;
        int dotLine = 0;
        boolean jobFound = false;

        //loop until the job status is published. Ignore other messages in queue.
        do {
            messages = sqs.receiveMessage(queueUrl).getMessages();
            if (dotLine++ < 20) {
                System.out.print(".");
            } else {
                System.out.println();
                dotLine = 0;
            }

            if (!messages.isEmpty()) {
                //Loop through messages received.
                for (Message message : messages) {
                    String notification = message.getBody();

                    // Get status and job id from notification.
                    ObjectMapper mapper = new ObjectMapper();
                    JsonNode jsonMessageTree = mapper.readTree(notification);
                    JsonNode messageBodyText = jsonMessageTree.get("Message");
                    ObjectMapper operationResultMapper = new ObjectMapper();
                    JsonNode jsonResultTree = operationResultMapper.readTree(messageBodyText.textValue());
                    JsonNode operationJobId = jsonResultTree.get("JobId");
                    JsonNode operationStatus = jsonResultTree.get("Status");
                    System.out.println("Job found was " + operationJobId);
                    // Found job. Get the results and display.
                    if (operationJobId.asText().equals(startJobId)) {
                        jobFound = true;
                        System.out.println("Job id: " + operationJobId);
                        System.out.println("Status : " + operationStatus.toString());
                        if (operationStatus.asText().equals("SUCCEEDED")) {
                            GetResultsFaceSearchCollection();
                        } else {
                            System.out.println("Video analysis failed");
                        }

                        sqs.deleteMessage(queueUrl, message.getReceiptHandle());
                    } else {
                        System.out.println("Job received was not job " + startJobId);
                        //Delete unknown message. Consider moving message to dead letter queue
                        sqs.deleteMessage(queueUrl, message.getReceiptHandle());
                    }
                }
            }
        } while (!jobFound);
        System.out.println("Done!");
    }


    private static void StartFaceSearchCollection(String bucket, String video) throws Exception{
        StartFaceSearchRequest req = new StartFaceSearchRequest()
                .withCollectionId(COLLECTION_ID)
                .withVideo(new Video()
                        .withS3Object(new S3Object()
                                .withBucket(bucket)
                                .withName(video)))
                .withNotificationChannel(channel);

        StartFaceSearchResult startPersonCollectionSearchResult = rek.startFaceSearch(req);
        startJobId=startPersonCollectionSearchResult.getJobId();
        storeRunningInDB(startJobId);
    }

    private static void GetResultsFaceSearchCollection() throws Exception {
        GetFaceSearchResult faceSearchResult=null;
        int maxResults = 10;
        String paginationToken=null;
        List<Map<String, ?>> resultData = new ArrayList<>();
        do {
            if (faceSearchResult !=null){
                paginationToken = faceSearchResult.getNextToken();
            }

            faceSearchResult  = rek.getFaceSearch(
                    new GetFaceSearchRequest()
                            .withJobId(startJobId)
                            .withMaxResults(maxResults)
                            .withNextToken(paginationToken)
                            .withSortBy(FaceSearchSortBy.TIMESTAMP)
            );

            //Show search results

            List<PersonMatch> matches = faceSearchResult.getPersons();
            for (PersonMatch match: matches) {
                List <FaceMatch> faceMatches = match.getFaceMatches();
                if (faceMatches != null) {
                    long milliSeconds = match.getTimestamp();
                    System.out.print("Timestamp: " + Long.toString(milliSeconds));
                    System.out.println(" Person number: " + match.getPerson().getIndex());
                    for (FaceMatch faceMatch: faceMatches){
                        Face face = faceMatch.getFace();
                        System.out.println("Face Id: "+ face.getFaceId());
                        System.out.println("Similarity: " + faceMatch.getSimilarity().toString());
                        BoundingBox box = face.getBoundingBox();
                        System.out.printf("Box: top=%s left=%s height=%s width=%s%n", box.getTop(), box.getLeft(), box.getHeight(), box.getWidth());
                        System.out.println();
                        resultData.add(makeFaceResult(faceMatch, match.getTimestamp()));
                    }
                }
            }
        } while (faceSearchResult !=null && faceSearchResult.getNextToken() != null);
        storeFinishedInDB(startJobId, resultData);
    }

    private static Map<String, ?> makeFaceResult(FaceMatch faceMatch, long timestamp) {
        Face face = faceMatch.getFace();
        Map<String, Object> result = new HashMap<>();
        result.put("faceId", face.getFaceId());
        result.put("similarity", faceMatch.getSimilarity().toString());
        result.put("timestamp", timestamp);
        result.put("box", boxToMap(face.getBoundingBox()));

        return result;
    }

    private static Map<String, Float> boxToMap(BoundingBox box) {
        Map<String, Float> result = new HashMap<>();
        result.put("top", box.getTop());
        result.put("left", box.getLeft());
        result.put("width", box.getWidth());
        result.put("height", box.getHeight());
        return result;
    }

    // DynamoDB
    private static final String dbTableName = "faceFinder";

    private static void storeRunningInDB(String jobId){
        AmazonDynamoDB client = Helpers.buildDynamoClient();
        DynamoDB dynamoDB = new DynamoDB(client);
        Table table = dynamoDB.getTable(dbTableName);
        table.deleteItem("ID",0);
        Item item = new Item()
                .withPrimaryKey("ID", 0)
                .withString("Status", "RUNNING")
                .withString("JobId", jobId)
                .withMap("Data", Collections.emptyMap());

        table.putItem(item);
    }

    private static void storeFinishedInDB(String jobId, List<Map<String, ?>> resultData) {
        AmazonDynamoDB client = Helpers.buildDynamoClient();
        DynamoDB dynamoDB = new DynamoDB(client);
        Table table = dynamoDB.getTable(dbTableName);
        table.deleteItem("ID",0);
        Item item = new Item()
                .withPrimaryKey("ID", 0)
                .withString("Status", "FINISHED")
                .withString("JobId", jobId)
                .withList("Data", resultData);

        table.putItem(item);
    }
}
