package ru.avareev.clouds.facefinder;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;

public class StatusResponse {
    @JsonProperty
    String jobId;
    @JsonProperty
    String status;
    @JsonProperty
    List<Map<String,?>> data;

    public StatusResponse(){}

    public StatusResponse(String jobId, String status, List<Map<String, ?>> data) {
        this.jobId = jobId;
        this.status = status;
        this.data = data;
    }
}
