package ru.avareev.clouds.facefinder;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FindFaceRequest {
    @JsonProperty
    String faceImage;
    @JsonProperty
    String video;

    public FindFaceRequest(){}
}
